[Oppgave 2: Tetromino-fabrikken](./05-OPG2-TETROMINOFABRIKKEN.md) &bullet; [README](../README.md) &bullet; [Oppgave 4: Paint](./07-OPG4-PAINT.md)

## Oppgave 3: Whack-a-mole misclick

I denne oppgaven skal du gjøre en endring i programmet *INF101 ColorWhack* som du finner i pakken no.uib.inf101.exam23v.colorwhack. Programmet fungerer allerede ganske bra: du kan starte spillet med å klikke på en rute; poenget er deretter å ha så mange svarte ruter som mulig til enhver tid, og prosenten som vises er den gjennomsnittlige andelen av svarte ruter du har klart å bevare så langt. Spillet er ferdig etter 30 sekunder, og den som har den høyeste prosenten da, vinner.

Direktøren i «The Whack-a-Mole Company» har imidlertid hyret deg inn som konsulent for å gjøre spillet enda vanskeligere. Hun ønsker følgende funksjonalitet: dersom brukeren klikker på en rute som allerede er svart, da skal alle de svarte rutene bli fargelagt med en tilfeldig farge. Vedlagt i oppgavebeskrivelsen til jobben har du fått en gif-animasjon som er laget av teamets UX-designer, og som demonstrerer ønsket funksjonalitet:

![ønsket funksjonalitet](./img/misclick-whack.gif)

Det er en hastejobb som visstnok betaler ganske bra, så du takket selvfølgelig ja til oppgaven umiddelbart.

Hint: Du kan bruke metoden `ColorWhackModel::getNewColor` for å velge hvilken farge du skal tegne med, denne metoden tar allerede hensyn til at fargevalget er mer begrenset i begynnelsen av spillet.