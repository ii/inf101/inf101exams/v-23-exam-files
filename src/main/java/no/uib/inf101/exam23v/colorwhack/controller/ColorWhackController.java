package no.uib.inf101.exam23v.colorwhack.controller;

import no.uib.inf101.exam23v.colorwhack.view.BoardView;
import no.uib.inf101.exam23v.colorwhack.view.ColorWhackMainView;
import no.uib.inf101.graphics.CellPositionToPixelConverter;
import no.uib.inf101.grid.CellPosition;

import javax.swing.JButton;
import javax.swing.Timer;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * A controller for the ColorWhack game. The controller listens for
 * events on the view, such as clicks on the board, and updates the
 * model accordingly. Furthermore, the controller has a timer that
 * periodically pops up new colors on the board, and reacts to
 * clicks on the reset button.
 */
public class ColorWhackController {
  
  private final ControllableWhackModel model;
  private final Timer popupTimer;

  /**
   * Creates a new controller for the ColorWhack game.
   *
   * @param model the model to control
   * @param view the view on which to listen for events
   */
  public ColorWhackController(ControllableWhackModel model, ColorWhackMainView view) {
    Objects.requireNonNull(model, "model cannot be null");
    Objects.requireNonNull(view, "view cannot be null");

    this.model = model;
    this.popupTimer = new Timer(0, null);

    this.setupMouseListenerOnBoard(view.getBoardView());
    this.setupPopupTimer();
    this.setupResetButton(view.getResetButton());
  }

  private void setupPopupTimer() {
    this.popupTimer.setInitialDelay(0);
    this.popupTimer.setDelay(this.model.getDelayBetweenPopups(LocalDateTime.now()));
    this.popupTimer.addActionListener(e -> {
      this.model.setNewColorOnRandomPosition(LocalDateTime.now());
      this.popupTimer.setDelay(this.model.getDelayBetweenPopups(LocalDateTime.now()));
    });
    this.popupTimer.start();
  }

  private void setupMouseListenerOnBoard(BoardView boardView) {
    boardView.addMouseListener(new MouseAdapter() {
      @Override
      public void mousePressed(MouseEvent event) {
        CellPositionToPixelConverter converter = boardView.getConverter();
        CellPosition pos = converter.getCellPositionOfPoint(event.getPoint());
        if (pos != null) {
          model.whack(pos, LocalDateTime.now());
        }
      }
    });
  }

  private void setupResetButton(JButton resetButton) {
    resetButton.addActionListener(event -> this.model.reset());
  }
}
