package no.uib.inf101.exam23v.colorwhack.model;

/** The state of the game. */
public enum GameState {
    WAITING, RUNNING, GAME_OVER
}
