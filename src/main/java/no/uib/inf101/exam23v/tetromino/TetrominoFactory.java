package no.uib.inf101.exam23v.tetromino;

/**
 * A factory for producing tetrominos.
 */
public interface TetrominoFactory {
  
  /** Returns the next tetromino to use. */
  Tetromino getNext();
}
