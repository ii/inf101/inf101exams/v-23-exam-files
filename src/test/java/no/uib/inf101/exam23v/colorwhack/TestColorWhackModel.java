package no.uib.inf101.exam23v.colorwhack;

import no.uib.inf101.eventbus.EventBus;
import no.uib.inf101.exam23v.colorwhack.model.ColorWhackModel;
import no.uib.inf101.exam23v.colorwhack.model.GameState;
import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.GridCell;
import no.uib.inf101.grid.IReadOnlyGrid;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.awt.Color;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

public class TestColorWhackModel {

  private static final LocalDateTime TIME_ONE = LocalDateTime.of(2021, 5, 1, 12, 0);
  private static final LocalDateTime TIME_TWO = TIME_ONE.plus(Duration.ofSeconds(1));
  private static final LocalDateTime TIME_THREE = TIME_TWO.plus(Duration.ofSeconds(1));
  private static final LocalDateTime TIME_FAR_FUTURE = TIME_THREE.plus(Duration.ofSeconds(29));

  private ColorWhackModel model;

  @BeforeEach
  void prepareStandardModel() {
    EventBus eventBus = new EventBus();
    Random random = new Random(42);
    this.model = new ColorWhackModel(eventBus, random);
  }

  @Test
  void gameStateIsWaiting() {
    assertEquals(GameState.WAITING, model.getGameState());
  }

  @Test
  void delayIsAlwaysFourHundred() {
    assertEquals(400, model.getDelayBetweenPopups(TIME_ONE));
    assertEquals(400, model.getDelayBetweenPopups(TIME_TWO));
    assertEquals(400, model.getDelayBetweenPopups(TIME_THREE));
  }

  @Test
  void centerTileIsRed() {
    IReadOnlyGrid<Color> board = model.getBoard();
    assertEquals(Color.RED, board.get(new CellPosition(2, 2)));
  }

  @Test
  void otherTilesAreBlack() {
    IReadOnlyGrid<Color> board = model.getBoard();
    assertEquals(Color.BLACK, board.get(new CellPosition(1, 2)));
    assertEquals(Color.BLACK, board.get(new CellPosition(2, 1)));
    assertEquals(Color.BLACK, board.get(new CellPosition(2, 3)));
    assertEquals(Color.BLACK, board.get(new CellPosition(3, 2)));
  }

  @Nested
  class ThenWhackCenterTileTimeOne {
    @BeforeEach
    void doASingleWhackAtCenter() {
      model.whack(new CellPosition(2, 2), TIME_ONE);
    }

    @Test
    void centerTileIsBlack() {
      IReadOnlyGrid<Color> board = model.getBoard();
      assertEquals(Color.BLACK, board.get(new CellPosition(2, 2)));
    }

    @Test
    void currentStateIsRunning() {
      assertEquals(GameState.RUNNING, model.getGameState());
    }

    @Test
    void scoreIsOneHundred() {
      assertEquals(100, model.getScorePercentage());
    }

    @Nested
    class ThenPopANewColor {
      @BeforeEach
      void popANewColor() {
        model.setNewColorOnRandomPosition(TIME_TWO);
      }

      @Test
      void gameStateIsRunning() {
        assertEquals(GameState.RUNNING, model.getGameState());
      }

      @Test
      void scoreIsStillOneHundred() {
        assertEquals(100, model.getScorePercentage());
      }

      @Test
      void thereIsOneRedTheRestAreBlack() {
        IReadOnlyGrid<Color> board = model.getBoard();
        int redCount = 0;
        for (GridCell<Color> gc : board) {
          if (Color.RED.equals(gc.value())) {
            redCount++;
          } else if (!Color.BLACK.equals(gc.value())) {
            fail("Only red and black should be on the board, but found " + gc.value());
          }
        }
        assertEquals(1, redCount);
      }

      @Nested
      class ThenWhackThatRedTileAgain {
        @BeforeEach
        void whackThatRedTileAgain() {
          // First find position of the red tile
          CellPosition redTile = null;
          IReadOnlyGrid<Color> board = model.getBoard();
          for (GridCell<Color> gc : board) {
            if (Color.RED.equals(gc.value())) {
              redTile = gc.pos();
              break;
            }
          }
          model.whack(redTile, TIME_THREE);
        }

        @Test
        void gameStateIsRunning() {
          assertEquals(GameState.RUNNING, model.getGameState());
        }

        @Test
        void scoreIsNowReducedByTwo() {
          assertEquals(98, model.getScorePercentage());
        }

        @Test
        void allTilesAreBlack() {
          IReadOnlyGrid<Color> board = model.getBoard();
          for (GridCell<Color> gc : board) {
            assertEquals(Color.BLACK, gc.value());
          }
        }

        @Test
        void timeIsFaster() {
          assertTrue(model.getDelayBetweenPopups(TIME_THREE) < 400);
        }

        @Nested
        class ThenHitReset {
          @BeforeEach
          void popANewColorAgain() {
            model.reset();
          }

          @Test
          void centerTileIsRed() {
            IReadOnlyGrid<Color> board = model.getBoard();
            assertEquals(Color.RED, board.get(new CellPosition(2, 2)));
          }

          @Test
          void otherTilesAreBlack() {
            IReadOnlyGrid<Color> board = model.getBoard();
            assertEquals(Color.BLACK, board.get(new CellPosition(1, 2)));
            assertEquals(Color.BLACK, board.get(new CellPosition(2, 1)));
            assertEquals(Color.BLACK, board.get(new CellPosition(2, 3)));
            assertEquals(Color.BLACK, board.get(new CellPosition(3, 2)));
          }

          @Test
          void gameStateIsWaiting() {
            assertEquals(GameState.WAITING, model.getGameState());
          }
        }

        @Nested
        class ThenPopNewStuffFarInFuture {
          @BeforeEach
          void popNewStuffFarInFuture() {
            model.setNewColorOnRandomPosition(TIME_FAR_FUTURE);
          }

          @Test
          void gameStateIsGameOver() {
            assertEquals(GameState.GAME_OVER, model.getGameState());
          }
        }
      }
    }
  }
}
